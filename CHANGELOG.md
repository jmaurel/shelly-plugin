# Changelog

## 4.6.10
- EM: fix reset total / total_returned url
## 4.6.9
- EM: add relay and status return webhook (VAR3)
## 4.6.8
- EM: fix channel identifier
## 4.6.7
- Plus1: add
- Pro1: add
## 4.6.6
- RGBW: fix action url
## 4.6.5
- RGBW: fix power meter
## 4.6.4
- Pro4PM: force polling power
- Plus1PM: force polling power
## 4.6.3
- Pro4PM: add channel 2/3/4
## 4.6.2
- Pro4PM : add
## 4.6.1
- fix 1PM power / total power
## 4.6.0
- add battery device
## 4.4.0
- add report_url door window (battery device)
## 4.3.0
- not use /status endpoint : Json to XML made in Eedomus is so :X
## 4.2.1
- Shelly i3 : fix return status value :P
## 4.2.0
- add Shelly i3
- fix all devices (not in battery) polling to 1 (min)
## 4.1.1
- config actions urls
## 4.1.0
- delete ALL periph state return -> **Launch [VAR3] into Web browser to initiliaze return state**
- add wait 3s before calling Shelly to update value
- add Shelly 1L device
- fix temperature addon xpath
- fix EM xpath
## 4.0.1
- fix setValue when as multiple periphId 
## 4.0.0
- add init and check (state return)
- add HTTP control to manage auto state return
- fix shelly EM canal 2 - fix #29
- add shelly button1 - fix #27
- fix 'return state' on Door Window1/2 - fix #26
- update screenshots
## 3.5.2
- fix XML node '|' char with show_config=1
## 3.5.1
- fix consommation shelly 2.5 relay
- fix polling of power and total
## 3.5.0
- add shelly RGBW2 (white mode)
- refactoring README
## 3.4.2
- refactoring xpath to convert '1' to '100'
- fix roller_pos syntax
## 3.4.1
- fix polling shelly 1PM
## 3.4.0
- add shelly RGBW2 (color mode)
## 3.3.0
- add shelly 3EM device
## 3.2.0
- add shelly EM device
## 3.1.0
- add tilt and vivration of shelly Door/Window
## 3.0.0
- add shelly Door/Window
- refactoring type identification (thanks to @thrymartin tips) to add more devices
- fix hidden values resolved by xpath 1.0 if/then/else
## 2.1.0
- add shelly Flood
## 2.0.1
- add shelly 2.5 roller mode
- refacto 'state return management' with shelly.php script
  - remove user/password for Eedomus API
  - add generic code to see show_config of periph
  - remove polling if necessary
  - remove additionnal rules of battery devices
## 1.0.2
- add temperature sensor addon (shelly 1/1PM)
## 1.0.1
- fix update url H&T [b]report_url[/b]
## 1.0.0
- delete all rules (always created ...)
- add state return management
- update README
## 0.3.1
- update documentation
## 0.3.0
- add [Shelly Plug / PlugS](https://shelly-api-docs.shelly.cloud/#shelly-plug-plugs)
- add documentation for [status feedback](documentation/status_feedback.md)
## 0.2.0
- add [Shelly Dimmer/SL](https://shelly-api-docs.shelly.cloud/#shelly-dimmer-sl)
## 0.1.1
- refactoring Shelly Relay: merge HTTP Control and HTTP Sensor
## 0.1.0
- add [Shelly H&T](https://shelly-api-docs.shelly.cloud/#shelly-h-amp-t)
- add [Shelly Smoke](https://shelly-api-docs.shelly.cloud/#shelly-smoke)
- refactoring Shelly Relay (shelly 1/1PM/2/2-5/4Pro)
- update README
## 0.0.2
- add 1 HTTP captor to show total consumption
- update README
## 0.0.1
- add 3 devices for each channel
- add 2 devices for each channel
- add LICENCE
- add README
- init project
